;;;; package.lisp
;;;;
;;;; Copyright (c) 2012-2015 Robert Smith

(defpackage #:symbolic-function
  (:use #:cl)

  ;; compilation-utilities.lisp
  (:export
   #:compile-and-load-string            ; FUNCTION
   #:compile-and-load-forms             ; FUNCTION
   #:compile-lambda-form                ; FUNCTION
   )

  ;; symbolic-function.lisp
  (:export
   #:symbolic-function                  ; CLASS
   #:compile-symbolic-function          ; FUNCTION
   #:make-symbolic-function             ; FUNCTION
   #:slambda                            ; MACRO
   #:define-symbolic-function           ; MACRO

   #:symbolic-compose                   ; FUNCTION
   #:function-arity                     ; FUNCTION
   #:bound-variable-p                   ; FUNCTION
   #:free-variable-p                    ; FUNCTION
   #:nullary-function-p                 ; FUNCTION
   #:unary-function-p                   ; FUNCTION
   #:binary-function-p                  ; FUNCTION
   #:alpha-convert                      ; FUNCTION
   )

  ;; differentiation.lisp
  (:export
   #:differentiate                      ; FUNCTION
   )
  )
