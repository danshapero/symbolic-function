;;;; symbolic-function.asd
;;;;
;;;; Copyright (c) 2012-2015 Robert Smith

(asdf:defsystem #:symbolic-function
  :description "A system for manipulating symbolic functions."
  :author "Robert Smith <quad@symbo1ics.com>"
  :license "BSD 3-Clause (See LICENSE)"
  :depends-on (#:closer-mop
               #:cl-fad
               #:optima
               #:fare-quasiquote-optima)
  :serial t
  :components ((:file "package")
               (:file "compilation-utilities")
               (:file "symbolic-function")
               (:file "differentiation")))
