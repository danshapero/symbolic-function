;;;; differentiation.lisp
;;;;
;;;; Copyright (c) 2012-2015 Robert Smith

(in-package #:symbolic-function)

(defun diff (expr var)
  "Differentiate the symbolic expression EXPR with respect to the variable VAR."
  (labels ((D (expr)
             (cond
               ((symbolp expr) (if (eq expr var) 1 0))
               ((atom expr)    0)
               (t (optima:match expr
                    ((list '+) 0)
                    ((cons '+ fs) `(+ ,@(mapcar #'D fs)))
                    ((list '-) 0)
                    ((cons '- fs) `(- ,@(mapcar #'D fs)))
                    ((list '*) 0)
                    ((list '* f) (D f))
                    ((list '* f g)
                     `(+ (* ,(D f) ,g)
                         (* ,f ,(D g))))
                    ((cons '* gs)
                     ;; (* a b c d) => (* a (* b (* c d)))
                     (D (reduce (lambda (x y) `(* ,x ,y))
                                gs
                                :from-end t)))
                    ((list '/ f) (D `(/ 1 ,f)))
                    ((list* '/ f gs)
                     (let ((g `(* ,@gs)))
                        `(/ (- (* ,g ,(D f))
                               (* ,f ,(D g)))
                            (expt ,g 2))))
                    ((list 'sqrt f)
                     (D `(expt ,f 1/2)))
                    ((list 'expt f 0)
                     (declare (ignore f))
                     0)
                    ((list 'expt f 1) (D f))
                    ((list 'expt f n) `(* ,(D f)
                                       ,n
                                       (expt ,f (- ,n 1))))
                    ((list 'log f) `(/ ,(D f) ,f))
                    ((list 'sin f) `(* ,(D f) (cos ,f)))
                    ((list 'cos f) `(- (* ,(D f) (sin ,f))))
                    ((list 'tan f) (D `(/ (sin ,f) (cos ,f))))
                    ((list 'exp f) `(* ,(D f) (exp ,f)))
                    (otherwise (error "Don't know how to diff ~S" expr)))))))
    (D expr)))

(defun differentiate (sf &optional (index 0))
  (let ((var (nth index (symbolic-function.parameters sf)))
        (expr (symbolic-function.expression sf)))
    (make-symbolic-function (copy-list (symbolic-function.parameters sf))
                            (diff expr var))))

(defun newton-iteration-function (f)
  ;; x - f(x)/f'(x)
  (sf- (slambda (x) x)
       (sf/ f (differentiate f))))

(defun root (f x0 &key (iterations 10))
  (let ((newton (newton-iteration-function f)))
    (loop :repeat (1+ iterations)
          :for x := x0 :then (funcall newton x)
          :finally (return x))))

(defun cool-sqrt (n)
  (let ((f (make-symbolic-function '(x) `(- (expt x 2) ,n))))
    (root f (/ n 2.0))))
