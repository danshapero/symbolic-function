;;;; compilation-utilities.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith

(in-package #:symbolic-function)

(defun compile-and-load-string (string)
  "Compile and load the string STRING of Lisp code."
  (let ((source (cl-fad:with-output-to-temporary-file (s)
                  (write-string string s)))
        (fasl (cl-fad:with-output-to-temporary-file (s)
                ;; We cannot ignore S here since WITH-... uses it.
                )))
    (unwind-protect (load (compile-file source :output-file fasl))
      (when (probe-file source)
        (delete-file source))
      (when (probe-file fasl)
        (delete-file fasl)))
    t))

(defun compile-and-load-forms (forms)
  "Compile and load the list of forms FORMS as if they were toplevel forms."
  (let ((source (cl-fad:with-output-to-temporary-file (s)
                  (with-standard-io-syntax
                    (dolist (form forms)
                      (print form s)
                      (terpri s)
                      (terpri s)))))
        (fasl (cl-fad:with-output-to-temporary-file (s)
                ;; We cannot ignore S here since WITH-... uses it.
                )))
    (unwind-protect (load (compile-file source :output-file fasl))
      (when (probe-file source)
        (delete-file source))
      (when (probe-file fasl)
        (delete-file fasl)))
    t))

(defun compile-lambda-form (lambda-form)
  "Compile the lambda form LAMBDA-FORM, whose shape is

    (LAMBDA (<argument>*)
      <body form>*),

into a compiled function."
  (compile nil lambda-form))
